package sesja;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/shop")
public class ShopServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        if (session.getAttribute("productList")!=null){
            List<Product> productList = (List<Product>) session.getAttribute("productList");
            resp.getWriter().println("<table border=\"3px\"><tr><td>Product Name</td><td>Price</td></tr>");
            for(Product product:productList){
                resp.getWriter().println("<tr><td>"+product.name+"</td><td>"+product.price+"</td></tr>");
            }
            resp.getWriter().println("</table>");
        }else {
            resp.getWriter().println("<p>No products added!</p>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String name = req.getParameter("name");
        Double price = Double.parseDouble(req.getParameter("price"));
        Product product = new Product(name,price);
        List<Product> productList;

        if(session.getAttribute("productList")==null){
            productList = new ArrayList<>();
        }else {
            productList = (List<Product>) session.getAttribute("productList");
        }

        productList.add(product);
        session.setAttribute("productList",productList);
    }
}
