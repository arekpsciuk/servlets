package ksiazki;

import java.util.Comparator;
import java.util.Objects;

public class Book {
    private String title;
    private String author;
    private int yearOfRelease;

    public Book(String title, String author, int yearOfRelease) {
        this.title = title;
        this.author = author;
        this.yearOfRelease = yearOfRelease;
    }

    class SortByTitle implements Comparator<Book>{
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }

        @Override
        public Comparator reversed() {
            return new SortByTitleReversed();
        }
    }

    class SortByTitleReversed implements Comparator<Book>{
        @Override
        public int compare(Book o1, Book o2) {
            return (o2.getTitle().compareTo(o1.getTitle()));
        }
    }

    class SortByAuthor implements Comparator<Book>{
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getAuthor().compareTo(o2.getAuthor());
        }

        @Override
        public Comparator reversed() {
            return new SortByAuthorReversed();
        }
    }

    class SortByAuthorReversed implements Comparator<Book>{
        @Override
        public int compare(Book o1, Book o2) {
            return (o2.getAuthor().compareTo(o1.getAuthor()));
        }
    }

    class SortByYear implements Comparator<Book>{
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getYearOfRelease()-o2.getYearOfRelease();
        }

        @Override
        public Comparator<Book> reversed() {
            return null;
        }
    }

    class SortByYearReversed implements Comparator<Book>{
        @Override
        public int compare(Book o1, Book o2) {
            return o2.getYearOfRelease()-o1.getYearOfRelease();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }
}
