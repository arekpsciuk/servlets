package ksiazki;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@WebServlet("/library")
public class LibraryServlet extends HttpServlet {
    private List<Book> bookList = new LinkedList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String search = req.getParameter("search");
        String sort = req.getParameter("sort");
        String descending = req.getParameter("desc");

        if (sort != null) {
            switch (sort) {
                case "author":
                    if (descending.equals("true")) {
                        bookList.sort(Comparator.comparing(Book::getAuthor).reversed());
                    }else {
                        bookList.sort(Comparator.comparing(Book::getAuthor));
                    }
                    break;
                case "year":
                    if (descending.equals("true")) {
                        bookList.sort(Comparator.comparing(Book::getYearOfRelease).reversed());
                    }else {
                        bookList.sort(Comparator.comparing(Book::getYearOfRelease));
                    }
                    break;
                default:
                    if (descending.equals("true")) {
                        bookList.sort(Comparator.comparing(Book::getTitle).reversed());
                    }else {
                        bookList.sort(Comparator.comparing(Book::getTitle));
                    }
            }
        }

        if (!(search == null)) {
            for (Book book : bookList) {
                if (book.getTitle().equals(search)) {
                    resp.getWriter().println("Title: " + book.getTitle() + ", Author: " + book.getAuthor() +
                            ", Year of release: " + book.getYearOfRelease() + "<br>");
                }
            }
        } else {
            for (Book book : bookList) {
                resp.getWriter().println("Title: " + book.getTitle() + ", Author: " + book.getAuthor() +
                        ", Year of release: " + book.getYearOfRelease() + "<br>");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String author = req.getParameter("author");
        int yearOfRelease = Integer.parseInt(req.getParameter("year"));

        bookList.add(new Book(title, author, yearOfRelease));
    }

    class SortByTitle implements Comparator<Book> {
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }

        @Override
        public Comparator reversed() {
            return new SortByTitleReversed();
        }
    }

    class SortByTitleReversed implements Comparator<Book> {
        @Override
        public int compare(Book o1, Book o2) {
            return (o2.getTitle().compareTo(o1.getTitle()));
        }
    }

    class SortByAuthor implements Comparator<Book> {
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getAuthor().compareTo(o2.getAuthor());
        }

        @Override
        public Comparator reversed() {
            return new SortByAuthorReversed();
        }
    }

    class SortByAuthorReversed implements Comparator<Book> {
        @Override
        public int compare(Book o1, Book o2) {
            return (o2.getAuthor().compareTo(o1.getAuthor()));
        }
    }

    class SortByYear implements Comparator<Book> {
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getYearOfRelease() - o2.getYearOfRelease();
        }

        @Override
        public Comparator<Book> reversed() {
            return new SortByYearReversed();
        }
    }

    class SortByYearReversed implements Comparator<Book> {
        @Override
        public int compare(Book o1, Book o2) {
            return o2.getYearOfRelease() - o1.getYearOfRelease();
        }
    }

}
