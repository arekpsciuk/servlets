package register;

import walidator.Validator;

import java.lang.reflect.Method;

public class User {
    private String name;
    private String lastName;
    private int age;
    private String email;
    private String zip;
    private String password;
    private StringBuilder errorMessage = new StringBuilder();

    public User(String name, String lastName, int age, String email, String zip, String password) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
        this.zip = zip;
        this.password = password;
    }

    public boolean isValid(){
        boolean isValid = true;
        if (!isNameValid()) isValid=false;
        if (!isLastNameValid()) isValid=false;
        if (!isAgeValid()) isValid=false;
        if (!isEmailValid()) isValid=false;
        if (!isZipValid()) isValid=false;
        if (!isPasswordValid()) isValid=false;
        return isValid;
    }

    private boolean isNameValid(){
        if(!(name==null)&&!Validator.validate("name",name)){
            errorMessage.append("<p>Incorrect name </p>");
            return false;
        }
        return true;
    }

    private boolean isLastNameValid(){
        if(!(lastName==null)&&!Validator.validate("name",lastName)){
            errorMessage.append("<p>Incorrect last name </p>");
            return false;
        }
        return true;
    }

    private boolean isAgeValid(){
        if (age<0||age>150){
            errorMessage.append("<p>Incorrect age</p>");
            return false;
        }
        return true;
    }

    private boolean isEmailValid(){
        if (!(email==null)&&!Validator.validate("email",email)){
            errorMessage.append("<p>Incorrect e-mail </p>");
            return false;
        }
        return true;
    }

    private boolean isZipValid(){
        if (!(zip==null)&&!Validator.validate("zip",zip)){
            errorMessage.append("<p>Incorrect zip code </p>");
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(){
        if (!(password==null)&&(!password.contains("!")||!password.contains("#"))){
            errorMessage.append("<p>Incorrect password </p>");
            return false;
        }
        return true;
    }

    public StringBuilder getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", zip='" + zip + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
