package register;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet("/userList")
public class UserList extends HttpServlet {
    public static List<User> users = new LinkedList<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StringBuilder builder = new StringBuilder("<p>");
        for (User user:users) {
            builder.append(user + "<br>");
        }
        builder.append("</p>");
        resp.getWriter().println(builder);
    }
}
