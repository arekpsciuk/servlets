package register;

import walidator.Validator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet("/registerOld")
public class RegisterServlet extends HttpServlet {

    UserList userList = new UserList();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastName");
        int age = -1;
        String email = req.getParameter("email");
        String zip = req.getParameter("zip");
        String password = req.getParameter("password");
        boolean isValid = true;

        try {
            age = Integer.parseInt(req.getParameter("age"));
        }catch (NumberFormatException e){
            System.out.println(e);
        }

        User user = new User(name,lastName,age,email,zip,password);

        if (user.isValid()){
            RequestDispatcher dispatcher = req.getRequestDispatcher("/userList");
            UserList.users.add(new User(name,lastName,age,email,zip,password));
            dispatcher.forward(req,resp);
        }else {
            RequestDispatcher dispatcher = req.getRequestDispatcher("registerForm.html");
            resp.getWriter().println(user.getErrorMessage());
            dispatcher.include(req,resp);
        }
    }
}
