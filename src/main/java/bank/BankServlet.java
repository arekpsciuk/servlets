package bank;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/bank")
public class BankServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("list") != null) {
            List<Transaction> transactions = (List<Transaction>) session.getAttribute("list");
            StringBuilder sb = new StringBuilder();
            sb.append("<p>Saldo: " + session.getAttribute("balance") + "</p>");
            sb.append("<table border=\"1px\"><tr><td>Typ Transakcji</td><td>Tytul</td><td>Data</td><td>Kwota</td></tr>");
            for (Transaction transaction : transactions) {
                sb.append("<tr><td>" + transaction.getType().type + "</td>");
                sb.append("<td>" + transaction.name + "</td>");
                sb.append("<td>" + transaction.date + "</td>");
                sb.append("<td>" + (transaction.value*transaction.getType().multiplier) + "</td></tr>");
            }
            sb.append("</table>");
            resp.getWriter().println(sb);
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("bank.html");
        dispatcher.include(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TransactionType type = null;
        Double value;
        String name;

        try {
            type = TransactionType.chooseType(req.getParameter("type"));
            value = Double.parseDouble(req.getParameter("value"));
            name = req.getParameter("name");
        } catch (Exception e) {
            System.out.println(e);
            resp.getWriter().println("<p>Niepoprawne dane</p>");
            RequestDispatcher dispatcher = req.getRequestDispatcher("bank.html");
            dispatcher.include(req, resp);
            return;
        }

        LocalDate date = LocalDate.now();
        Transaction transaction = new Transaction(type, value, date, name);

        HttpSession session = req.getSession();

        List<Transaction> transactions;

        if (session.getAttribute("list") == null) {
            transactions = new ArrayList<>();
        } else {
            transactions = (List<Transaction>) session.getAttribute("list");
        }

        transactions.add(transaction);
        session.setAttribute("list", transactions);

        Double balance;

        if (session.getAttribute("balance") == null) {
            balance = 0.0;
        } else {
            balance = (Double) session.getAttribute("balance");
        }

        balance += transaction.getValue() * transaction.getType().multiplier;

        session.setAttribute("balance", balance);
    }
}
