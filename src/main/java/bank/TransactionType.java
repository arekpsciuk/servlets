package bank;

public enum TransactionType {
    INCOME("Wplata",1),
    OUTCOME("Wyplata",-1);

    String type;
    Integer multiplier;

    TransactionType(String type, Integer multiplier) {
        this.type = type;
        this.multiplier = multiplier;
    }

    public static TransactionType chooseType(String type){
        switch (type){
            case "income":
                return INCOME;
            case "outcome":
                return OUTCOME;
            default:
                return null;
        }
    }
}
