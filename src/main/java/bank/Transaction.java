package bank;

import java.time.LocalDate;

public class Transaction {
    TransactionType type;
    Double value;
    LocalDate date;
    String name;

    public Transaction(TransactionType type, Double value, LocalDate date, String name) {
        this.type = type;
        this.value = value;
        this.date = date;
        this.name = name;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
