package waluty;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/waluty")
public class CurrencyExchangeServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String curr1 = req.getParameter("curr1");
        String curr2 = req.getParameter("curr2");
        Double value = Double.parseDouble(req.getParameter("value"));
        Conversion conversion = new Conversion(curr1, curr2, value);
        resp.getWriter().println(conversion.exchange() + curr1);
    }
}
