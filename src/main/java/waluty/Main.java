package waluty;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        Conversion conversion = new Conversion("USD","PLN",10.0);

        try {
            System.out.println(conversion.exchange());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
