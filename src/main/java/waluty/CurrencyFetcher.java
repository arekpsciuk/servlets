package waluty;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class CurrencyFetcher {
    private final static String EXCHANGE_RATE_API_URL = "https://api.exchangeratesapi.io/latest?base=";

    public Double getRate(String curr1, String curr2) {
        Double rate = 0.0;
        String url = String.format("%s%s", EXCHANGE_RATE_API_URL, curr1);
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            JsonElement element = JsonParser.parseReader(rd);
            System.out.println(element);
            rate = element.getAsJsonObject().get("rates").getAsJsonObject().get(curr2).getAsDouble();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rate;
    }
}
