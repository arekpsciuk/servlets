package waluty;

public class Conversion {

    private final String curr1;
    private final String curr2;
    private final Double value;
    private final CurrencyFetcher currencyFetcher = new CurrencyFetcher();

    Conversion(String curr1, String curr2, Double value) {
        this.curr1 = curr1;
        this.curr2 = curr2;
        this.value = value;
    }

    public Double exchange() {
        if (curr1.equals(curr2)) {
            return value;
        }

        return value * currencyFetcher.getRate(curr1,curr2);
    }
}
