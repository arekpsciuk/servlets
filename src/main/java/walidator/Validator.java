package walidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public enum Validator {
    EMAIL("email", Pattern.compile("\\w+@\\w+\\.\\w+")),
    ZIP("zip", Pattern.compile("[0-9]{2}-[0-9]{3}")),
    PHONE("phone", Pattern.compile("\\+48[0-9]{9}")),
    NAME("name",Pattern.compile("[A-Z].*"));

    private String type;
    private Pattern pattern;

    Validator(String type, Pattern pattern) {
        this.type = type;
        this.pattern = pattern;
    }

    public static Boolean validate (String type, String input){
        return Stream.of(values())
                .filter((Validator validator)->validator.type.equals(type))
                .map((Validator validator)->validator.checkPattern(input))
                .findAny()
                .orElse(false);
    }

    private Boolean checkPattern(String input){
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }
}
