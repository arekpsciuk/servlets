package copy;

public class Director implements Cloneable {
    String name;
    String lastName;
    String filmType;

    public Director(String name, String lastName, String filmType) {
        this.name = name;
        this.lastName = lastName;
        this.filmType = filmType;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Director{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", filmType='" + filmType + '\'' +
                '}';
    }
}
