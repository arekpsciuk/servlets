package copy;

public class Copy {
    public static void main(String[] args) {
        Film original = new Film("Title1", 1990,
                new Director("Name1", "Last Name 1", "Film Type 1"));

        Film shallowCopy = null;
        Film deepCopy = null;

        try {
            shallowCopy = (Film) original.clone();
            deepCopy = (Film) original.deepCopy();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println(original);
        System.out.println(shallowCopy);
        System.out.println(deepCopy);
        System.out.println("---------Change Title-----------");
        original.title = "Title2";
        System.out.println(original);
        System.out.println(shallowCopy);
        System.out.println(deepCopy);
        System.out.println("---------Change Director Name-------------");
        original.director.name = "Name 2";
        System.out.println(original);
        System.out.println(shallowCopy);
        System.out.println(deepCopy);
    }
}
