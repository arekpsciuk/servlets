package copy;

public class Film implements Cloneable {
    String title;
    int yearOfRelease;
    Director director;

    public Film(String title, int yearOfRelease, Director director) {
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.director = director;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    protected Object deepCopy() throws CloneNotSupportedException {
        Film cloneFilm = (Film) super.clone();
        Director cloneDirector = (Director) director.clone();
        cloneFilm.director = cloneDirector;
        return cloneFilm;
    }

    @Override
    public String toString() {
        return "Film{" +
                "title='" + title + '\'' +
                ", yearOfRelease=" + yearOfRelease +
                ", director=" + director +
                '}';
    }
}
