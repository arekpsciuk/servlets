package login;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("login.html");
        dispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        User user = new User(login,email,password);

        if (!user.isValidLogin()) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("login.html");
            resp.getWriter().println(user.getErrorMessage());
            dispatcher.include(req, resp);
        }else if(!RegisterServlet.users.contains(user)){
            RequestDispatcher dispatcher = req.getRequestDispatcher("register.html");
            resp.getWriter().println("<p>User doesn't exist! Register first.</p>");
            dispatcher.include(req,resp);
        }else if (user.getEmail().equals(email)&&user.getPassword().equals(password)){
            RequestDispatcher dispatcher = req.getRequestDispatcher("welcome.html");
            dispatcher.forward(req,resp);
        }else {
            RequestDispatcher dispatcher = req.getRequestDispatcher("login.html");
            resp.getWriter().println("<p>Incorrect login, e-mail or password</p>");
            dispatcher.include(req,resp);
        }
    }
}
