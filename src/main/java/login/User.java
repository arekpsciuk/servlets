package login;

import walidator.Validator;

import java.lang.reflect.Method;
import java.util.Objects;

public class User {
    private String login;
    private String name;
    private String lastName;
    private String email;
    private String password;
    private StringBuilder errorMessage = new StringBuilder();

    public User(String login, String name, String lastName, String email, String password) {
        this.login = login;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public User(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    public boolean isValidRegister(){
        boolean isValid = true;
        if (!isLoginValid()) isValid = false;
        if (!isNameValid()) isValid=false;
        if (!isLastNameValid()) isValid=false;
        if (!isEmailValid()) isValid=false;
        if (!isPasswordValid()) isValid=false;
        return isValid;
    }

    public boolean isValidLogin(){
        boolean isValid = true;
        if (!isLoginValid()) isValid = false;
        if (!isEmailValid()) isValid=false;
        if (!isPasswordValid()) isValid=false;
        return isValid;
    }

    private boolean isLoginValid() {
        if (!(login==null)){
            return login.equals(login.toLowerCase());
        }
        return false;
    }

    private boolean isNameValid(){
        if(!(name==null)&&!Validator.validate("name",name)){
            errorMessage.append("<p>Incorrect name </p>");
            return false;
        }
        return true;
    }

    private boolean isLastNameValid(){
        if(!(lastName==null)&&!Validator.validate("name",lastName)){
            errorMessage.append("<p>Incorrect last name </p>");
            return false;
        }
        return true;
    }

    private boolean isEmailValid(){
        if (!(email==null)&&!Validator.validate("email",email)){
            errorMessage.append("<p>Incorrect e-mail </p>");
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(){
        if (password==null||(!password.contains("!")&&!password.contains("@")&&!password.contains("#"))){
            errorMessage.append("<p>Incorrect password </p>");
            return false;
        }
        return true;
    }

    public StringBuilder getErrorMessage() {
        return errorMessage;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }
}
