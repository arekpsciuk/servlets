package login;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    static List<User> users = new LinkedList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("register.html");
        dispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String name= req.getParameter("name");
        String lastName= req.getParameter("lastName");
        String email= req.getParameter("email");
        String password= req.getParameter("password");

        User user = new User(login,name,lastName,email,password);

        if (!user.isValidRegister()){
            RequestDispatcher dispatcher = req.getRequestDispatcher("register.html");
            resp.getWriter().println(user.getErrorMessage());
            dispatcher.include(req,resp);
        }else if(users.contains(user)){
            RequestDispatcher dispatcher = req.getRequestDispatcher("login.html");
            resp.getWriter().println("<p>User already in database</p>");
            dispatcher.include(req,resp);
        }else {
            RequestDispatcher dispatcher = req.getRequestDispatcher("welcome.html");
            users.add(user);
            dispatcher.forward(req,resp);
        }
    }
}
