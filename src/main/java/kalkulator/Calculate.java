package kalkulator;

@FunctionalInterface
public interface Calculate {
    Double calc(Double n1, Double n2);
}
