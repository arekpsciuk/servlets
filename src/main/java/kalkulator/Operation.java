package kalkulator;

import java.util.stream.Stream;

public enum Operation {
    ADD("+", (Double n1, Double n2)-> n1+n2),
    MULTI("*",(Double n1,Double n2)->n1*n2),
    DIV("/", (Double n1,Double n2)->n1/n2),
    SUB("-",(Double n1, Double n2)->n1-n2),
    MOD("%", (Double n1, Double n2)->n1%n2);


    private String op;
    private Calculate calculate;

    Operation(String operation, Calculate calculate) {
        this.op = operation;
        this.calculate = calculate;
    }

    public static Double calculate(String op, Double n1, Double n2){
        return Stream.of(values())
                .filter((Operation operation)-> operation.op.equals(op))
                .map((Operation operation)-> operation.calculate.calc(n1,n2))
                .findAny()
                .orElse(Double.MIN_VALUE);
    }
}
