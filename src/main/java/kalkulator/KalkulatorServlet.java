package kalkulator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/kalkulator")
public class KalkulatorServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Double value1 = Double.parseDouble(req.getParameter("value1"));
        Double value2 = Double.parseDouble(req.getParameter("value2"));
        String operation = req.getParameter("operation");
        Double result = Operation.calculate(operation, value1, value2);
        resp.getWriter().println(result);
    }
}
